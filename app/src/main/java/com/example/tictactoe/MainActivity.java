package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{


//        player representation
//        0 - X
//        1 - o

    int activePlayer = 0;
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};

//    Game states:
//    0 - X
//    1 - o
//    2 -  null

    int[][] winPos = {
                        {0,1,2},{3,4,5},{6,7,8},
                        {0,3,6},{1,4,7},{2,5,8},
                        {0,4,8},{2,4,6}
                     };



    public void onTap(View view)
    {
        TextView statusBar = findViewById(R.id.status);



        ImageView img = (ImageView) view;                                                            // taking reference to the object
        int selectedIndex = Integer.parseInt(img.getTag().toString());                               // taking its index

        if(gameState[selectedIndex] == 2)                                                            // if tapped cell is empty
        {
            gameState[selectedIndex] = activePlayer;
            img.setTranslationY(-1000f);

            if(activePlayer == 0)
            {
                statusBar.setText("O's Turn");
                img.setImageResource(R.drawable.x);
                activePlayer = 1;
            }
            else if(activePlayer == 1)
            {
                statusBar.setText("X's Turn");
                img.setImageResource(R.drawable.o);
                activePlayer = 0;
            }

            img.animate().translationYBy(1000f).setDuration(300);
        }




    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
